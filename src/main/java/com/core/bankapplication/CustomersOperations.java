package com.core.bankapplication;

import com.core.bankapplication.utils.ConnectionUtil;
import com.core.bankapplication.utils.IdGenerator;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Valeriy on 08.02.2017.
 */
public class CustomersOperations {

    private static void printCustomer(ResultSet result) {
        try {
            long id = result.getLong("id");
            String firstname = result.getString("firstname");
            String lastname = result.getString("lastname");
            Date birthdate = result.getDate("brthdate");
            String address = result.getString("address");
            String city = result.getString("city");
            String passport = result.getString("passport");
            String phone = result.getString("phone");

            System.out.println("ID: " + id + ". FirstName: " + firstname + ". LastName: " + lastname +
                    ". BirthDate: " + birthdate + ". Address: " + address + ". City: " + city +
                    ". Passport: " + passport + ". Phone: " + phone + ".");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void selectAllCustomers() {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    printCustomer(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Long> selectCustomersId(Connection connection) {
        List<Long> ids = new ArrayList<>();
        try {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("SELECT id FROM customers");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    ids.add(resultSet.getLong("ID"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ids;
    }

    public static void selectCustomersByPassport(String passport) {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM customers WHERE passport=?");
                statement.setString(1, passport);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    printCustomer(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void constantInsert() {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                Statement statement = connection.createStatement();
                int updateCount = statement.executeUpdate("INSERT INTO customers " +
                        "(id,firstname,lastname,brthdate,address,city)" +
                        "VALUES (3,'Vasya','Petrov','1985-09-03','Dnipro','Dnipro')");
                System.out.println("Inserted " + updateCount + " row.");
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    public static void smartInsert(String firstname, String lastname, String birthdate,
                                   String address, String city, String passport, String phone) {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO customers " +
                        "(firstname,lastname,brthdate,address,city,passport,phone)" +
                        " VALUES (?,?,?,?,?,?,?)");
                statement.setString(1, firstname);
                statement.setString(2, lastname);
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-mm-yyyy");
                java.util.Date date = sdf1.parse(birthdate);
                java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
                statement.setDate(3, sqlStartDate);
                statement.setString(4, address);
                statement.setString(5, city);
                statement.setString(6, passport);
                statement.setString(7, phone);
                // statement.setLong(8, IdGenerator.getGeneratedId(statement));
                statement.executeUpdate();
            }
        }
        catch (ParseException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateCustomerFirstAndLastName(Long id, String firstName, String lastName) {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("UPDATE customers SET firstname=?,lastname=? WHERE id=?");
                statement.setString(1, firstName);
                statement.setString(2, lastName);
                statement.setLong(3, id);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteCustomer(Long id){
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM customers WHERE id=?");
                statement.setLong(1, id);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
