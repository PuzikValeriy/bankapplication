package com.core.bankapplication;

import com.core.bankapplication.AccountsOperations;
import com.core.bankapplication.CustomersOperations;
import com.core.bankapplication.utils.ConnectionUtil;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valeriy on 09.02.2017.
 */
public class TransactionsOperations {

    private static void printTransaction(ResultSet result) {
        try {
            long id = result.getLong("id");
            BigDecimal amount = result.getBigDecimal("amount");
            Timestamp date = result.getTimestamp("date");
            String operationtype = result.getString("operationtype");
            long accountnumber = result.getLong("accountnumber");

            System.out.println("ID: " + id + ". Amount: " + amount + ". Date: " + date +
                    ". OperationType: " + operationtype + ". AccountNumber: " + accountnumber + ".");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void selectTransactions() {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM transactions");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    printTransaction(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void batchTransactionsInsert(){
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                List<Long> ids = AccountsOperations.selectAccountsId(connection);
                PreparedStatement statement = connection.prepareStatement("INSERT INTO transactions " +
                        "(amount,date,operationtype,accountnumber)" +
                        " VALUES (?,?,?,?)");
                for (Long id : ids) {
                    statement.setBigDecimal(1, new BigDecimal(100));
                    statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                    statement.setString(3, "PUT");
                    statement.setLong(4, id);
                    statement.addBatch();
                }
                statement.executeBatch();   
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateAllTransactions(){
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
                ResultSet resultSet = statement.executeQuery("SELECT id,amount,operationtype FROM transactions");
                while (!resultSet.isLast()) {
                    resultSet.next();
                    String operationtype = resultSet.getString("operationtype");
                    if ("PUT".equals(operationtype)) {
                        resultSet.updateBigDecimal("amount", new BigDecimal(150));
                        resultSet.updateRow();
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteAllWithdraw(){
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
                ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions");
                while (!resultSet.isLast()) {
                    resultSet.next();
                    String operationtype = resultSet.getString("operationtype");
                    if ("WSD".equals(operationtype)) {
                        long id = resultSet.getLong("accountnumber");
                        resultSet.deleteRow();
                        resultSet.moveToInsertRow();
                        resultSet.updateBigDecimal("amount",new BigDecimal(100));
                        resultSet.updateTimestamp("date",new Timestamp(System.currentTimeMillis()));
                        resultSet.updateString("operationtype","PUT");
                        resultSet.updateLong("accountnumber",id);
                        resultSet.insertRow();
                        resultSet.moveToCurrentRow();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
