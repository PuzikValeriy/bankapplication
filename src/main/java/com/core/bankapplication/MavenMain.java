package com.core.bankapplication;

/**
 * Created by Valeriy on 09.02.2017.
 */
public class MavenMain {
    public static void main(String[] args) {
        System.out.println("Customers:");
        CustomersOperations.selectAllCustomers();
        System.out.println('\n'+"Accounts:");
        AccountsOperations.selectAccounts();
        System.out.println('\n'+"Transactions:");
        TransactionsOperations.selectTransactions();

    }
}
