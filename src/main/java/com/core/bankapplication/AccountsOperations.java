package com.core.bankapplication;

import com.core.bankapplication.utils.ConnectionUtil;
import org.postgresql.util.PGTimestamp;

import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valeriy on 09.02.2017.
 */
public class AccountsOperations {

    private static void printAccount(ResultSet result) {
        try {
            long id = result.getLong("accountnumber");
            Double balance = result.getDouble("balance");
            Timestamp creationdate = result.getTimestamp("creationdate");
            String currency = result.getString("currency");
            boolean blocked = result.getBoolean("blocked");
            long customerid = result.getLong("customerid");

            System.out.println("AccountNumber: " + id + ". Balance: " + balance + ". CreationDate: " + creationdate +
                    ". Currency: " + currency + ". Blocked: " + blocked + ". CustomerID: " + customerid + ".");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void selectAccounts() {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM accounts");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    printAccount(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Long> selectAccountsId(Connection connection) {
        List<Long> ids = new ArrayList<>();
        try {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("SELECT accountnumber FROM accounts");
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    ids.add(resultSet.getLong("accountnumber"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ids;
    }

    public static void smartInsert(BigDecimal balance, String creationDate, String currency, boolean blocked, Long userId) {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO accounts " +
                        "(balance,creationdate,currency,blocked,customerid)" +
                        " VALUES (?,?,?,?,?)");
                statement.setBigDecimal(1, balance);
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                java.util.Date date = sdf1.parse(creationDate);
                java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(date.getTime());
                statement.setTimestamp(2, sqlTimestamp);
                statement.setString(3, currency);
                statement.setBoolean(4, blocked);
                statement.setLong(5, userId);
                statement.executeUpdate();
            }


        } catch (SQLException | ParseException e1) {
            e1.printStackTrace();
        }
    }

    public static void batchAccountsInsert() {
        try (Connection connection = ConnectionUtil.createConnection()) {
            if (connection != null) {
                List<Long> ids = CustomersOperations.selectCustomersId(connection);
                PreparedStatement statement = connection.prepareStatement("INSERT INTO accounts " +
                        "(balance,creationdate,currency,blocked,customerid)" +
                        " VALUES (?,?,?,?,?)");
                for (Long id : ids) {
                    statement.setBigDecimal(1, new BigDecimal(0));
                    statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                    statement.setString(3, "UAH");
                    statement.setBoolean(4, false);
                    statement.setLong(5, id);
                    statement.addBatch();
                }
                statement.executeBatch();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
