package com.homework;

import com.sun.istack.internal.NotNull;

/**
 * Created by Valeriy on 10.10.2016.
 */
public class Account implements Comparable<Account>{
    private Long accountNumber=0L;
    private Double balance;
    public static long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;


    public Account(Long accountNumber, Client client, Double balance) {
        this.accountNumber = GLOBAL_ACCOUNT_NUMBER;
        GLOBAL_ACCOUNT_NUMBER++;
        this.balance = balance;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public static long getGlobalAccountNumber() {
        return GLOBAL_ACCOUNT_NUMBER;
    }

    @Override
    public int compareTo(Account account){
        if(accountNumber<account.accountNumber) return -1;
        else if (accountNumber>account.accountNumber) return 1;
        else return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (accountNumber != null ? !accountNumber.equals(account.accountNumber) : account.accountNumber != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return accountNumber != null ? accountNumber.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Account: " + accountNumber +
                ", balance=$" + balance + ";";
    }

    public void increaseBalance(Double sum){
        this.balance+=sum;
    }

    public void decreaseBalance(Double sum){
        this.balance-=sum;
    }
}
