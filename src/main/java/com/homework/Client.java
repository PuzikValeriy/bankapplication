package com.homework;

import  java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valeriy on 10.10.2016.
 */
public class Client {

    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private Date dateOfBirth;
    private List<Account> accounts = new ArrayList<>();

    public Client(String firstName,String lastName,String address,String phone,Date dateOfBirth){
        this.firstName=firstName;
        this.lastName=lastName;
        this.address=address;
        this.phone=phone;
        this.dateOfBirth=dateOfBirth;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Account addAccount(Account account){
        if(accounts.add(account)) return account;
        return null;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (address != null ? !address.equals(client.address) : client.address != null) return false;
        if (dateOfBirth != null ? !dateOfBirth.equals(client.dateOfBirth) : client.dateOfBirth != null) return false;
        if (firstName != null ? !firstName.equals(client.firstName) : client.firstName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Client={" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", accounts:\n" +accounts.toString()+
                '}';
    }
}
